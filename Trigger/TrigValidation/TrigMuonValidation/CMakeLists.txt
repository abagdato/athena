################################################################################
# Package: TrigMuonValidation
################################################################################

# Declare the package name:
atlas_subdir( TrigMuonValidation )

# Install files from the package:
atlas_install_joboptions( share/TrigMuonValidation_RTT_*.py share/TrigMuonValidation_CreateEfficiencies.py )

